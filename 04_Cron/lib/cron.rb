require 'json'

class Cron
  def initialize
  end

  def self.valid_config?(conf)
  end

  def self.valid_run_at? (run_at)
  end

  def start
    loop do
      jobs.each do |job|
        execute(job) if should_be_executed?(job)
      end
      sleep 1
    end
  end

  def execute job
  end

  def run_command command
  end

  def save_log(job, output)
  end

  def check_log_dir
  end

  def get_log_filename(job)
  end

  def should_be_executed? job
  end

  def get_job_time(job)
  end
end

class Time
  def truncate_to_minute
    Time.new(self.year, self.month, self.day, self.hour, self.min, 0)
  end

  def truncate_to_hour
    Time.new(self.year, self.month, self.day, self.hour, 0, 0)
  end

  def truncate_to_day
    Time.new(self.year, self.month, self.day, 0, 0, 0)
  end

  def truncate_to_month
    Time.new(self.year, self.month, 1, 0, 0, 0)
  end
end
