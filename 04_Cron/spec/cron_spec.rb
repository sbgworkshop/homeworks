require_relative '../lib/cron'
require 'timecop'

describe "Cron" do
  let(:config) {
    [
      {
        run_at: '2014-01-25 00:00:00',
        command: 'ls * >> ~/tmp/ls'
      },
      {
        run_at: '2014-01-05 00:00:00',
        command: 'mail -c'
      }
    ]
  }

  def stub_config_load validation = true
    config_path = File.expand_path("~/.rcron.json")
    File.should_receive(:exists?).with(config_path).and_return(true)
    File.should_receive(:open).with(config_path).and_return(:mock_file_pointer)
    JSON.should_receive(:load).with(:mock_file_pointer, nil, {symbolize_names: true}).and_return(config)
    Cron.should_receive(:valid_config?).with(config).and_return(validation)
  end

  describe "initialization" do
    it "raises error when config file is missing" do
      config_path = File.expand_path("~/.rcron.json")
      File.should_receive(:exists?).with(config_path).and_return(false)
      expect { Cron.new }.to raise_error(RuntimeError, "Config file is missing")
    end

    it "loads and validates json config file" do
      stub_config_load
      Cron.new
    end

    it "raises exception when config is invalid" do
      stub_config_load false
      expect { Cron.new }.to raise_error(RuntimeError, "Bad config format")
    end
  end

  describe "config" do
    it "is array of jobs" do
      Cron.valid_config?([]).should be_true
      Cron.valid_config?({}).should be_false
    end

    it "each job is hash" do
      Cron.valid_config?(config).should be_true
      config << 1
      Cron.valid_config?(config).should be_false
    end

    it "each job has :run_at and :command keys" do
      Cron.valid_config?(config).should be_true
      config << {foo: 'bar', baz: 'boo'}
      Cron.valid_config?(config).should be_false
    end

    describe "run_at" do
      let(:config) {[
        {
          run_at: '',
          command: 'ls * >> ~/tmp/ls'
        }
      ]}

      it "is validated" do
        Cron.should respond_to(:valid_run_at?)
      end

      it "can be exact time" do
        config.first[:run_at] = '2014-01-02 23:44:12'
        Cron.valid_config?(config).should be_true

        config.first[:run_at] = '2014-01-02 fofooo'
        Cron.valid_config?(config).should be_false
      end

      it "can be every minute" do
        config.first[:run_at] = 'every minute'
        Cron.valid_config?(config).should be_true

        config.first[:run_at] = 'every minute my man'
        Cron.valid_config?(config).should be_false
      end

      it "can be every hour" do
        config.first[:run_at] = 'every hour'
        Cron.valid_config?(config).should be_true
        config.first[:run_at] = 'every hour man'
        Cron.valid_config?(config).should be_false
        config.first[:run_at] = 'almost every hour'
        Cron.valid_config?(config).should be_false
      end

      it "can be every day" do
        config.first[:run_at] = 'every day'
        Cron.valid_config?(config).should be_true
        config.first[:run_at] = 'every day man'
        Cron.valid_config?(config).should be_false
        config.first[:run_at] = 'almost every day'
        Cron.valid_config?(config).should be_false
      end

      it "can be every month" do
        config.first[:run_at] = 'every month'
        Cron.valid_config?(config).should be_true
        config.first[:run_at] = 'every month man'
        Cron.valid_config?(config).should be_false
        config.first[:run_at] = 'almost every month'
        Cron.valid_config?(config).should be_false
      end
    end
  end

  describe "execute" do
    let(:output) { "file1.txt\nfile2.txt\nfile3.txt" }
    let(:job) {{run_at: 'every hour', command: 'ls -l'}}

    before(:each) do
      stub_config_load
      @cron = Cron.new
    end

    it "runs command and saves output to log" do
      @cron.should_receive(:run_command).with(job[:command]).and_return(output)
      @cron.should_receive(:save_log).with(job, output).and_return(true)
      @cron.execute(job)
    end

    describe "run_command" do
      it "runs command and returns output" do
        io = double(IO)
        File.should_receive(:popen).with(job[:command]).and_return(io)
        io.should_receive(:gets).and_return(output)

        @cron.run_command(job[:command]).should == output
      end
    end

    describe "save_log" do

      describe "check_log_dir" do

        it "checks and creates log directory if needed" do
          log_dir = File.expand_path("~/.rcron_logs/")
          Dir.should_receive(:exists?).with(log_dir).and_return(false)
          Dir.should_receive(:mkdir).with(log_dir)
          @cron.check_log_dir
        end
      end

      describe "get_log_name" do
        before(:each) do
          stub_config_load
          @cron = Cron.new
        end

        it "exact time log name" do
          Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
            job = {run_at: '2014-01-24 05:30:00', command: 'ls'}
            log_filename = File.join(
              File.expand_path("~/.rcron_logs"),
              "2014_01_24_05_30_00_ls.log"
            )
            @cron.get_log_filename(job).should == log_filename
          end
        end

        it "every minute log name" do
          Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
            job = {run_at: 'every minute', command: 'ls'}
            log_filename = File.join(
              File.expand_path("~/.rcron_logs"),
              "2014_01_24_05_30_00_ls.log"
            )
            @cron.get_log_filename(job).should == log_filename
          end
        end

        it "every hour log name" do
          Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
            job = {run_at: 'every hour', command: 'ls'}
            log_filename = File.join(
              File.expand_path("~/.rcron_logs"),
              "2014_01_24_05_00_00_ls.log"
            )
            @cron.get_log_filename(job).should == log_filename
          end
        end

        it "every day log name" do
          Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
            job = {run_at: 'every day', command: 'ls'}
            log_filename = File.join(
              File.expand_path("~/.rcron_logs"),
              "2014_01_24_00_00_00_ls.log"
            )
            @cron.get_log_filename(job).should == log_filename
          end
        end

        it "every month log name" do
          Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
            job = {run_at: 'every month', command: 'ls'}
            log_filename = File.join(
              File.expand_path("~/.rcron_logs"),
              "2014_01_01_00_00_00_ls.log"
            )
            @cron.get_log_filename(job).should == log_filename
          end
        end

        it "returns filename based on time and command" do
          Timecop.freeze(Time.new(2014, 01, 24, 15, 44, 12)) do
            log_dir = File.expand_path("~/.rcron_logs/")
            filename = '2014_01_24_15_00_00_ls_-l.log'
            @cron.get_log_filename(job).should == File.join(log_dir, filename)
          end
        end
      end

      it "saves output to file" do
        Timecop.freeze do
          log_dir = File.expand_path("~/.rcron_logs/")
          filename = Time.new.strftime('%Y_%m_%d_%M_%H_%S_ls_-l.log')
          log_file = File.join(log_dir, filename)
          @cron.should_receive(:check_log_dir).and_return(true)
          @cron.should_receive(:get_log_filename).with(job).and_return(log_file)
          file_double = double(File)
          File.should_receive(:open).with(log_file, 'w').and_return(file_double)
          file_double.should_receive(:puts).with(output)
          file_double.should_receive(:close)
          @cron.save_log(job, output)
        end
      end
    end
  end

  describe "should_be_executed?" do
    before(:each) do
      stub_config_load
      @cron = Cron.new
    end

    describe "checks for exact time execution" do
      it "should be executed" do
        Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
          job = {run_at: '2014-01-24 05:30:00', command: 'ls'}
          log_filename = File.join(
            File.expand_path("~/.rcron_logs"),
            "2014_01_24_05_30_00_ls.log"
          )
          File.should_receive(:exists?).with(log_filename).and_return(false)
          @cron.should_be_executed?(job).should == true
        end
      end

      it "should not be executed" do
        Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
          job = {run_at: '2014-01-24 05:30:00', command: 'ls'}
          log_filename = File.join(
            File.expand_path("~/.rcron_logs"),
            "2014_01_24_05_30_00_ls.log"
          )
          File.should_receive(:exists?).with(log_filename).and_return(true)
          @cron.should_be_executed?(job).should == false
        end
      end
    end

    describe "checks for minute execution" do
      it "should be executed" do
        Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
          job = {run_at: 'every minute', command: 'ls'}
          log_filename = File.join(
            File.expand_path("~/.rcron_logs"),
            "2014_01_24_05_30_00_ls.log"
          )
          File.should_receive(:exists?).with(log_filename).and_return(false)
          @cron.should_be_executed?(job).should == true
        end
      end

      it "should not be executed" do
        Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
          job = {run_at: 'every minute', command: 'ls'}
          log_filename = File.join(
            File.expand_path("~/.rcron_logs"),
            "2014_01_24_05_30_00_ls.log"
          )
          File.should_receive(:exists?).with(log_filename).and_return(true)
          @cron.should_be_executed?(job).should == false
        end
      end
    end

    describe "checks for hour execution" do
      it "should be executed" do
        Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
          job = {run_at: 'every hour', command: 'ls'}
          log_filename = File.join(
            File.expand_path("~/.rcron_logs"),
            "2014_01_24_05_00_00_ls.log"
          )
          File.should_receive(:exists?).with(log_filename).and_return(false)
          @cron.should_be_executed?(job).should == true
        end
      end

      it "should not be executed" do
        Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
          job = {run_at: 'every hour', command: 'ls'}
          log_filename = File.join(
            File.expand_path("~/.rcron_logs"),
            "2014_01_24_05_00_00_ls.log"
          )
          File.should_receive(:exists?).with(log_filename).and_return(true)
          @cron.should_be_executed?(job).should == false
        end
      end
    end

    describe "checks for day execution" do
      it "should be executed" do
        Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
          job = {run_at: 'every day', command: 'ls'}
          log_filename = File.join(
            File.expand_path("~/.rcron_logs"),
            "2014_01_24_00_00_00_ls.log"
          )
          File.should_receive(:exists?).with(log_filename).and_return(false)
          @cron.should_be_executed?(job).should == true
        end
      end

      it "should not be executed" do
        Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
          job = {run_at: 'every day', command: 'ls'}
          log_filename = File.join(
            File.expand_path("~/.rcron_logs"),
            "2014_01_24_00_00_00_ls.log"
          )
          File.should_receive(:exists?).with(log_filename).and_return(true)
          @cron.should_be_executed?(job).should == false
        end
      end
    end

    describe "checks for month execution" do
      it "should be executed" do
        Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
          job = {run_at: 'every month', command: 'ls'}
          log_filename = File.join(
            File.expand_path("~/.rcron_logs"),
            "2014_01_01_00_00_00_ls.log"
          )
          File.should_receive(:exists?).with(log_filename).and_return(false)
          @cron.should_be_executed?(job).should == true
        end
      end

      it "should not be executed" do
        Timecop.freeze(Time.new(2014, 01, 24, 5, 30, 13)) do
          job = {run_at: 'every month', command: 'ls'}
          log_filename = File.join(
            File.expand_path("~/.rcron_logs"),
            "2014_01_01_00_00_00_ls.log"
          )
          File.should_receive(:exists?).with(log_filename).and_return(true)
          @cron.should_be_executed?(job).should == false
        end
      end
    end
  end
end
