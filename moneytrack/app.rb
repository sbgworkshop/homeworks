require 'sinatra'
require 'sinatra/flash'
require 'data_mapper'

enable :sessions

configure do
  set :views, File.expand_path("./app/views/")
  set :session_secret, 'this is my big secret'
end

# DataMapper::Logger.new($stdout, :debug)
DataMapper.setup(:default, 'mysql://root@localhost/moneytrack')

Dir.glob("./app/models/*.rb").each { |file| require file }
Dir.glob("./app/controllers/*.rb").each { |file| require file }

DataMapper.finalize.auto_upgrade!
