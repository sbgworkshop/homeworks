require 'capybara/cucumber'
require_relative '../../app'

Capybara.app = Sinatra::Application

require 'database_cleaner'
require 'database_cleaner/cucumber'

DatabaseCleaner.strategy = :truncation
