Given(/^I click on "(.*?)"$/) do |link|
  click_on(link)
end

Given(/^I enter "(.*?)" as "(.*?)"$/) do |value, field|
  fill_in(field, with: value)
end


And(/^I created account "([^"]*)" with "([^"]*)" balance$/) do |name, balance|
  steps %{
    Given that I opened MoneyTrack home page
    And I click on "Accounts"
    And I click on "Create Account"
    And I enter "#{name}" as "Name"
    And I enter "#{balance}" as "Starting balance"
    When I click on "Create"
    Then I should see message "Created: #{name}"
  }
end

Then(/^I should see "([^"]*)" with "([^"]*)" balance listed$/) do |account, balance|
  find('tr', text: account).should have_content(balance)
end

And(/^my total balance should be "([^"]*)"$/) do |balance|
  find('tr', text: 'Total balance').should have_content(balance)
end

And(/^I click "([^"]*)" of "([^"]*)" account$/) do |action, account|
  find(:xpath, "//tr[td[contains(.,'#{account}')]]/td/a", :text => action).click
end