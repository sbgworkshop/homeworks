Given(/^that I opened MoneyTrack home page$/) do
  visit('/')
end

Then(/^I should see message "(.*?)"$/) do |message|
  has_content?(message).should == true
end
