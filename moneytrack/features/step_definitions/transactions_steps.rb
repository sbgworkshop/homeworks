Then(/^account total balance should be "([^"]*)"$/) do |total_balance|
  find('tr', text: 'Total balance').should have_content(total_balance)
end

Then(/^I should see "([^"]*)" with amount "([^"]*)"$/) do |name, amount|
  find('tr', text: name).should have_content(amount)
end