Feature: Money Accounts
  As a MoneyTrack user
  I want to have multiple accounts
  So I can track my financial status

  Scenario: Creating an Account
    Given that I opened MoneyTrack home page
    And I click on "Accounts"
    And I click on "Create Account"
    And I enter "My bank account" as "Name"
    And I enter "100" as "Starting balance"
    When I click on "Create"
    Then I should see message "Created: My bank account"

  Scenario: Failed creating an Account
    Given that I opened MoneyTrack home page
    And I click on "Accounts"
    And I click on "Create Account"
    When I click on "Create"
    Then I should see message "Name must not be blank"
    And I should see message "Balance must be a number"

  Scenario: Listing Accounts
    Given that I opened MoneyTrack home page
    And I created account "My bank account" with "100" balance
    And I created account "Savings" with "500" balance
    When I click on "Accounts"
    Then I should see "My bank account" with "100" balance listed
    And I should see "Savings" with "500" balance listed
    And my total balance should be "600"

  Scenario: Editing account
    Given that I opened MoneyTrack home page
    And I created account "My bank account" with "100" balance
    And I click "Edit" of "My bank account" account
    And I enter "Bank account" as "Name"
    And I enter "200" as "Starting balance"
    When I click on "Save"
    Then I should see message "Updated: Bank account"
    And I should see "Bank" with "200" balance listed

  Scenario: Failed editing an Account
    Given that I opened MoneyTrack home page
    And I created account "My bank account" with "100" balance
    And I click "Edit" of "My bank account" account
    And I enter "" as "Name"
    And I enter "not a number" as "Starting balance"
    When I click on "Save"
    Then I should see message "Name must not be blank"
    And I should see message "Balance must be a number"

  Scenario: Deleting an Account
    Given that I opened MoneyTrack home page
    And I created account "My bank account" with "100" balance
    And I created account "Savings" with "500" balance
    When I click "Delete" of "My bank account" account
    Then I should see message "Deleted: My bank account"
    And I should see "Savings" with "500" balance listed
    And my total balance should be "500"
