Feature: Transactions
  As a MoneyTrack user
  I want to track all of my transactions
  So I can track my financial status

  Scenario: Adding a expense
    Given that I opened MoneyTrack home page
    And I created account "My bank account" with "100" balance
    And I click on "My bank account"
    And I click on "Add expense"
    And I enter "Books" as "Name"
    And I enter "50" as "Amount"
    When I click on "Add"
    Then I should see "Books" with amount "-50"
    And account total balance should be "50"

  Scenario: Adding a income
    Given that I opened MoneyTrack home page
    And I created account "My bank account" with "100" balance
    And I click on "My bank account"
    And I click on "Add income"
    And I enter "Salary" as "Name"
    And I enter "5000" as "Amount"
    When I click on "Add"
    Then I should see "Salary" with amount "5000"
    And account total balance should be "5100"