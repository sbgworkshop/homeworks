require_relative '../simple_number'

describe SimpleNumber do
  describe "initialize" do
    it "raises exception when value passed is not a number" do
      expect{SimpleNumber.new('foo')}.to raise_error(RuntimeError)
    end

    it "does not raises exception when value passed is a number" do
      expect{SimpleNumber.new(4)}.not_to raise_error
    end
  end

  describe "adding" do
    it "adds numbers" do
      num = SimpleNumber.new(4)
      num.add(5).should == 9
    end

    it "adds negative numbers" do
      num = SimpleNumber.new(-2)
      num.add(-3).should == -5
      num.add(2).should == 0
    end

    it "allows method chaining" do
      num = SimpleNumber.new(2)
      num.add(2).add(3).should == 7
    end
  end

  describe "multiplication" do
    it "multiplies numbers" do
      num = SimpleNumber.new(2)
      num.multiply(3).should == 6
    end

    it "multiplies negative numbers" do
      num = SimpleNumber.new(-2)
      num.multiply(-3).should == 6
      num.multiply(5).should == -10
    end

    it "allows method chaining" do
      num = SimpleNumber.new(2)
      num.multiply(2).multiply(3).should == 12
    end
  end
end
