require_relative '../lib/grep'

describe Grep do
  describe "options" do
    describe "single options should work" do
      it "simple search: grep search_string file_list" do
        argv = ["search_string", "controller.rb", "connector.rb"]
        options = Grep.parse_options(argv)
        options.size.should == 2
        options['simple_search'].should == "search_string"
        options['files'].should == ["controller.rb", "connector.rb"]
      end

      it "showing file name: grep -f search_string file_list" do
        argv = ["-f", "search_string", "controller.rb", "connector.rb"]

        options = Grep.parse_options(argv)
        options.size.should == 3
        options["filename_show"].should == true
        options['simple_search'].should == "search_string"
        options['files'].should == ["controller.rb", "connector.rb"]
      end

      it "showing line number: grep -n search_string file_list" do
        argv = ["-n", "search_string", "controller.rb", "connector.rb"]

        options = Grep.parse_options(argv)
        options.size.should == 3
        options["line_numbers"].should == true
        options['simple_search'].should == "search_string"
        options['files'].should == ["controller.rb", "connector.rb"]
      end

      it "invert search: grep -v search_string file_list" do
        argv = ["-v", "search_string", "controller.rb", "connector.rb"]

        options = Grep.parse_options(argv)
        options.size.should == 3
        options["invert"].should == true
        options['simple_search'].should == "search_string"
        options['files'].should == ["controller.rb", "connector.rb"]
      end

      it "regular expressions: grep -e \"[0-9]*a\" file_list" do
        argv = ["-e", "[0-9]*a", "controller.rb", "connector.rb"]

        options = Grep.parse_options(argv)
        options.size.should == 2
        options['regexp'].should == "[0-9]*a"
        options['files'].should == ["controller.rb", "connector.rb"]
      end
    end

    describe "multiple options should work" do
      it "grep -fvn search_string file_list" do
        argv = ["-fvn", "search_string", "controller.rb", "connector.rb"]

        options = Grep.parse_options(argv)
        options.size.should == 5
        options["filename_show"].should == true
        options["invert"].should == true
        options["line_numbers"].should == true
        options['simple_search'].should == "search_string"
        options['files'].should == ["controller.rb", "connector.rb"]
      end

      it "grep -fve \"[0-9]a.*\" file_list" do
        argv = ["-fvn", "-e", "[0-9]a.*", "controller.rb", "connector.rb"]

        options = Grep.parse_options(argv)
        options.size.should == 5
        options["filename_show"].should == true
        options["invert"].should == true
        options["line_numbers"].should == true
        options['regexp'].should == "[0-9]a.*"
        options['files'].should == ["controller.rb", "connector.rb"]
      end
    end
  end

  describe "behavior" do
    let(:lines) {[
        "this is just a example text",
        "spawning across multiple lines",
        "which can be content from any textual file",
        "which is sent as argument to grep"
    ]}

    describe "Grep#grep_lines(lines, options, filename)" do
      describe "filters with simple search" do
        it "should find lines matching simple search" do
          # file1.rb and file2.rb will obviously be ignored as we are filtering lines from array
          argv = ["is", "file1.txt", "file2.txt"]
          matched = Grep.grep_lines(lines, Grep.parse_options(argv))
          matched.size.should == 2
          matched[0].should == "this is just a example text"
          matched[1].should == "which is sent as argument to grep"
        end

        it "should not find anything" do
          argv = ["no match found", "file1.txt", "file2.txt"]
          matched = Grep.grep_lines(lines, Grep.parse_options(argv))
          matched.size.should == 0
        end

        it "should not find anything when no search string is given" do
          argv = []
          matched = Grep.grep_lines(lines, Grep.parse_options(argv))
          matched.size.should == 0
        end
      end

      describe "showing line number" do
        it "should show line numbers for matched lines" do
          argv = ["-n", "is", "file1.txt", "file2.txt"]
          matched = Grep.grep_lines(lines, Grep.parse_options(argv))
          matched.size.should == 2
          matched[0].should == "1: this is just a example text"
          matched[1].should == "4: which is sent as argument to grep"
        end
      end

      describe "showing file name" do
        it "should show filename for matched lines" do
          argv = ["-f", "is", "file1.txt"]
          matched = Grep.grep_lines(lines, Grep.parse_options(argv), "file1.txt")
          matched.size.should == 2
          matched[0].should == "file1.txt: this is just a example text"
          matched[1].should == "file1.txt: which is sent as argument to grep"
        end

        it "should raise exception when there is no filename given" do
          argv = ["-f", "is", "file1.txt", "file2.txt"]
          expect {Grep.grep_lines(lines, Grep.parse_options(argv))}.to raise_error(RuntimeError, "No filename")
        end
      end

      describe "invert search" do
        it "should show lines not matching search" do
          argv = ["-v", "is", "file1.txt", "file2.txt"]
          matched = Grep.grep_lines(lines, Grep.parse_options(argv))
          matched.size.should == 2
          matched[0].should == "spawning across multiple lines"
          matched[1].should == "which can be content from any textual file"
        end
      end

      describe "regular expression" do
        it "should return lines matching given regular expression" do
          argv = ["-e", "^which .*", "file1.txt", "file2.txt"]
          matched = Grep.grep_lines(lines, Grep.parse_options(argv))
          matched.size.should == 2
          matched[0].should == "which can be content from any textual file"
          matched[1].should == "which is sent as argument to grep"
        end

        it "should return nothing when regular expression is empty" do
          argv = ["-e", "", "file1.txt", "file2.txt"]
          matched = Grep.grep_lines(lines, Grep.parse_options(argv))
          matched.size.should == 0
        end
      end

      describe "multiple options" do
        it "grep -fnv -e \"^which .*\" file1.txt" do
          argv = ["-fnv", "-e", "^which .*", "file1.txt"]
          matched = Grep.grep_lines(lines, Grep.parse_options(argv), "file1.txt")
          matched.size.should == 2
          matched[0].should == "file1.txt:1: this is just a example text"
          matched[1].should == "file1.txt:2: spawning across multiple lines"
        end

        it "grep -fnv is file1.txt" do
          argv = ["-fnv", "is", "file1.txt"]
          matched = Grep.grep_lines(lines, Grep.parse_options(argv), "file1.txt")
          matched.size.should == 2
          matched[0].should == "file1.txt:2: spawning across multiple lines"
          matched[1].should == "file1.txt:3: which can be content from any textual file"
        end
      end
    end

    describe "Grep#grep_file(filename, options)" do
      it "should raise error if given file does not exists" do
        file = "this_is_non_existing_file_on_my_filesystem.txt"
        expect{Grep.grep_file(file, [])}.to raise_error(RuntimeError, "File does not exists: #{file}")
      end

      it "read lines from given file and calls Grep#grep_lines()" do
        File.should_receive(:exists?).with('file1.txt').and_return(true)
        File.should_receive(:readlines).with('file1.txt').and_return(lines)

        argv = ["-fnv", "-e", "^which .*", "file1.txt"]
        matched = Grep.grep_file('file1.txt', Grep.parse_options(argv))
        matched.size.should == 2
        matched[0].should == "file1.txt:1: this is just a example text"
        matched[1].should == "file1.txt:2: spawning across multiple lines"
      end
    end

    describe "Grep#grep(arguments)" do
      it "calls Grep#grep_file for every file in options" do
        File.should_receive(:exists?).with('file1.txt').and_return(true)
        File.should_receive(:readlines).with('file1.txt').and_return(lines)
        File.should_receive(:exists?).with('file2.txt').and_return(true)
        File.should_receive(:readlines).with('file2.txt').and_return(lines)

        argv = ["-fnv", "-e", "^which .*", "file1.txt", "file2.txt"]

        matched = Grep.grep(argv)
        matched.size.should == 4
        matched[0].should == "file1.txt:1: this is just a example text"
        matched[1].should == "file1.txt:2: spawning across multiple lines"
        matched[2].should == "file2.txt:1: this is just a example text"
        matched[3].should == "file2.txt:2: spawning across multiple lines"
      end
    end
  end

end
